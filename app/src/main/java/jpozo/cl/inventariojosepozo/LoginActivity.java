package jpozo.cl.inventariojosepozo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    public Button btnAceptar;
    public EditText userIn;
    public EditText passIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Usuario usuario = new Usuario();



        btnAceptar = (Button) findViewById(R.id.btnIngresar);
        userIn = (EditText)findViewById(R.id.edtxtUsuario);
        passIn = (EditText)findViewById(R.id.edtxtPassword);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String valorUsuario = userIn.getText().toString();
                String valorPassword= passIn.getText().toString();

                Boolean stateUser = usuario.validaCredencial(valorUsuario,valorPassword);
                Log.d("TRAZA01", stateUser.toString());

                //Toast.makeText(LoginActivity.this, "stateUser: " + stateUser, Toast.LENGTH_LONG).show();
                if(stateUser == true){
                    Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                    //Bundle parmetros = new Bundle();
                    i.putExtra("usuario",valorUsuario);
                    startActivity(i);
                }
                else{
                    Toast.makeText(LoginActivity.this, "Credencial Incorrecta", Toast.LENGTH_LONG).show();
                }


            }
        });





    }
}
