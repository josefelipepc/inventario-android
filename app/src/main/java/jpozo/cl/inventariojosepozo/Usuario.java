package jpozo.cl.inventariojosepozo;

import java.util.ArrayList;

public class Usuario {
    String usuario, password, nombre, apellido, departamento;
    ArrayList<Usuario> usuarios = new ArrayList<>();

    public Usuario() {

        usuarios.add(new Usuario("jpozo", "123", "Jose", "Pozo", "Informatica"));
        usuarios.add(new Usuario("pulloa", "123", "Pedro", "Ulloa", "Informatica"));
        usuarios.add(new Usuario("visita1", "123", "Visita", "Visita", "Otro"));

    }


    public Usuario(String usuario, String password, String nombre, String apellido, String departamento) {
        this.usuario = usuario;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.departamento = departamento;
    }

    /**public void cargaUsuariosDefault() {

        usuarios.add(new Usuario("jpozo", "123", "Jose", "Pozo", "Informatica"));
        usuarios.add(new Usuario("pulloa", "123", "Pedro", "Ulloa", "Informatica"));
        usuarios.add(new Usuario("visita1", "123", "Visita", "Visita", "Otro"));
    }**/

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Boolean validaCredencial(String usuario, String password) {
        Integer totalUsuariosReg = usuarios.size();
        Integer contador = 0;
        for (contador = 0; contador <= totalUsuariosReg; contador++) {
            if (usuarios.get(contador).getUsuario().equals(usuario) && usuarios.get(contador).getPassword().equals(password)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getSaludoUsuario(String usuario){

        String saludo="";
        Integer totalUsuariosReg = usuarios.size();
        Integer contador = 0;
        for (contador = 0; contador <= totalUsuariosReg; contador++) {
            if (usuarios.get(contador).getUsuario().equals(usuario)) {
                saludo = usuarios.get(contador).getNombre() + " " + usuarios.get(contador).getApellido();
                return saludo;
                //break;
            } else {
                saludo = "Error User";
                return saludo;
            }
        }
        return saludo;
    }

    public String getDepartamentoUsuario(String usuario){

        String departamento="";
        Integer totalUsuariosReg = usuarios.size();
        Integer contador = 0;
        for (contador = 0; contador <= totalUsuariosReg; contador++) {
            if (usuarios.get(contador).getUsuario().equals(usuario)) {
                departamento = usuarios.get(contador).getDepartamento();
                return departamento;
                //break;
            } else {
                departamento = "Error Departamento";
                return departamento;
            }
        }
        return departamento;
    }
}
