package jpozo.cl.inventariojosepozo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //Toast.makeText(MenuActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
        String user = getIntent().getExtras().getString("usuario");
        Usuario usuario = new Usuario();

        String saludoUsuario = usuario.getSaludoUsuario(user);

        TextView txtViewSaludo = (TextView) findViewById(R.id.txtViewSaludo);
        TextView txtViewDepartamento = (TextView) findViewById(R.id.txtViewDeptoResult);
        txtViewSaludo.setText(saludoUsuario.toString());
        txtViewDepartamento.setText(usuario.getDepartamentoUsuario(user));
        Toast.makeText(MenuActivity.this, "Bienvenido " + saludoUsuario, Toast.LENGTH_LONG).show();

    }


}
