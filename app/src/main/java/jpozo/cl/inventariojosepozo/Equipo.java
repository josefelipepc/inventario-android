package jpozo.cl.inventariojosepozo;

import java.util.ArrayList;

public class Equipo {
    String serie;
    String descripcion;
    Integer valor;
    String estado;
    ArrayList<Equipo> equipos = new ArrayList<>();

    public Equipo(String serie, String descripcion, Integer valor,String estado) {
        this.serie = serie;
        this.descripcion = descripcion;
        this.valor = valor;
        this.estado = estado;
    }
    public void cargaEquipoDefault() {
        equipos.add(new Equipo("TEC-111", "Teclado", 10000,"Disponible"));
        equipos.add(new Equipo("TEC-112", "Teclado", 10000,"Disponible"));
        equipos.add(new Equipo("TEC-113", "Teclado", 10000,"Disponible"));
        equipos.add(new Equipo("MOU-211", "Mouse", 5000,"Disponible"));
        equipos.add(new Equipo("MOU-212", "Mouse", 5000,"Disponible"));
        equipos.add(new Equipo("MON-311", "Monitor", 100000,"Disponible"));
        equipos.add(new Equipo("NOT-411", "Notebook HP", 570000,"Disponible"));
        equipos.add(new Equipo("NOT-412", "Notebook Asus", 890000,"Disponible"));
        equipos.add(new Equipo("PEN-232", "Pendrive 8G", 7000,"Disponible"));
        equipos.add(new Equipo("PEN-233", "Pendrive 8G", 7000,"Disponible"));
        equipos.add(new Equipo("PEN-234", "Pendrive 8G", 7000,"Disponible"));
        equipos.add(new Equipo("PEN-235", "Pendrive 8G", 7000,"Disponible"));
        equipos.add(new Equipo("PEN-236", "Pendrive 8G", 7000,"Disponible"));
        equipos.add(new Equipo("VCA-444", "WebCam", 16000,"Disponible"));
        equipos.add(new Equipo("PRO-001", "Proyector Epson", 550000,"Disponible"));
        equipos.add(new Equipo("PRO-002", "Proyector Epson", 550000,"Disponible"));
        equipos.add(new Equipo("PRO-003", "Proyector Epson", 550000,"Disponible"));
        equipos.add(new Equipo("MON-233", "Monitor LG", 100000,"Disponible"));
        equipos.add(new Equipo("TAB-546", "Tablet Asus", 200000,"Disponible"));
        equipos.add(new Equipo("TAB-547", "Tablet Asus", 200000,"Disponible"));
        equipos.add(new Equipo("TAB-548", "Tablet Asus", 200000,"Disponible"));
        equipos.add(new Equipo("WAC-555", "Wacom", 800000,"Disponible"));
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String agregaEquipoNuevo(String serie,String descripcion,Integer valor){
        Integer totalEquipos = equipos.size();
        Integer contador=0;
        String mensajaEstado=null;
        for(contador=0;contador<=totalEquipos;contador++){
            if(equipos.get(contador).getSerie().equals(serie)){
                mensajaEstado = "Serie NOK";
            }
            else{
                equipos.add(new Equipo(serie, descripcion, valor,"Disponible"));
                mensajaEstado = "OK";
            }
        }
        return mensajaEstado;

    }

}
